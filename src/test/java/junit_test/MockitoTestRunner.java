package junit_test;


import mockito_test.Bar;
import mockito_test.BarVoid;
import mockito_test.Foo;
import mockito_test.FooVoid;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class MockitoTestRunner {
    private Foo foo;
    private Bar bar;

    @BeforeEach
    public void init (){
        foo = mock(Foo.class);
        bar = new Bar(foo);
    }

    @Test
    @DisplayName("Mockito test")
    public void test (){
        bar.bar("sql");
        //Assertions.assertEquals(bar.bar2("sql"),"sql2");
        verify(foo).foo("sql");
    }
    @Test
    @DisplayName("Mockito test2")
    public void test2 (){
        assertEquals(bar.bar2("sql"),"sql2");
    }

    @Test
    @DisplayName("Mockito test any string")
    public void test3 (){
        bar.bar("sqlm");
        verify(foo).foo(anyString());
    }

    @Test
    @DisplayName("Mockito test add return parameter")
    public void stubTest(){
        when (foo.foo("sql")).thenReturn("nosql");
        assertEquals(foo.foo("sql"),"nosql");
    }

    @Test
    @DisplayName("Mockito test void method")
    public void testExcluding (){
        FooVoid foo = mock(FooVoid.class);
        BarVoid bar = new BarVoid(foo);
        doThrow(new IllegalArgumentException()).when(foo).foo1(anyString());
        doNothing().when(foo).foo1("sql");

        bar.bar("sql");
        verify(foo).foo1("sql");

        //bar.bar("ss");
    }

    @Test
    @DisplayName("Mockito test with spy")
    public void testSpy (){
        Foo foo = spy(Foo.class);
        Bar bar = new Bar(foo);

        when(foo.foo("sql")).thenReturn("sql");
        assertEquals("sql",bar.bar("sql"));
    }

    @Test
    @DisplayName("Mockito test capture")
    public void testCapture (){
        Foo foo = spy(Foo.class);
        Bar bar = new Bar(foo);
        bar.bar("sql");
        bar.bar("ssd");
        bar.bar("sms");



        ArgumentCaptor <String> argument = ArgumentCaptor.forClass(String.class);
        verify(foo,times(3)).foo(argument.capture());
        assertEquals("[sql, ssd, sms]",argument.getAllValues().toString());
    }
}
