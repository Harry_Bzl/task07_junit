package junit_test;

import org.junit.jupiter.api.*;

class OperationsTest {

    private Operations operations;
    @BeforeAll
    static void printMessage (){
        System.out.println("Start test!");
    }
    @BeforeEach
    void before(TestInfo info){
        operations = new Operations();
        System.out.println("beforeEach: " + info.getDisplayName());
    }
    @DisplayName("Test method number with number = 1")
    @Test
     void testNumber(){
        String answer = operations.number(1);
        Assertions.assertEquals(answer, "1");
    }

    @DisplayName("Test method number with number = 0")
    @Test
    void testNumberZero(){
        Exception e = Assertions.assertThrows(IllegalArgumentException.class,
                () -> operations.number(0));
        Assertions.assertEquals(e.getMessage(),"exeption", "invalid message");
    }

    @Disabled ("for checking in range")
    @DisplayName("Test method float multiplication with number = 1.1")

    @Test
    void testFloatMultiplication(){
        double num;
        num = 1.1;
        double answer = operations.doubleMultiplication(num);
        Assertions.assertEquals(answer, 3.3);
    }

    @DisplayName("Test method float multiplication with word = 'hello' ")
    @Test
    void testX(){
        String word = operations.addX("hello");
        Assertions.assertTrue (word.contains("x")&&!word.equals(null));
    }
    @Tag("slow")
    @DisplayName("Test method float multiplication with value = 3, size 4")
    @Test
    void testArray(){
        int [] answer = operations.array(3,4);
        Assertions.assertArrayEquals(answer, new int[] {3,4,5,6});
    }
    @Tag("critical")
    @DisplayName("multipleFaulure")
    @Test
    void testAssertFail (){
        Assertions.assertAll(()->Assertions.assertTrue(!"exec".equals(operations.returnWord())),
                ()->Assertions.assertFalse(operations.returnWord().contains("x")));
    }

    @DisplayName("Test return word ")
    @Test
    void testThowRuntimeExeption(){
        Assertions.assertFalse("exec".equals(operations.returnWord()));
    }

    @AfterEach
    private void setNull(){
        operations = null;
    }



}