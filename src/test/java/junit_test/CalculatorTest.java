package junit_test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

class CalculatorTest {
    Calculator calculator = new Calculator();

    @TestFactory
    Stream <DynamicTest> dynamicTestStream(){
        Random random = new Random();
        return Stream.generate(Math::random).mapToInt(v->(int)(v*1000)).limit(10)
                .mapToObj(i->dynamicTest("test"+i,()->
                        assertTrue(calculator.sum(i,i)>=0)));
    }

    @ParameterizedTest
    @ValueSource (ints = {1,2,3,4,5})
    void sqrt (int i){
        Assertions.assertEquals(calculator.sqrt(i),i*i);
    }

    @ParameterizedTest
    @CsvSource({"2,3", "3,-1", "-3,5"})
    void multipleSum (int i,int y){
        Assertions.assertTrue(calculator.sum(i,y)>0);
    }

    Stream<Arguments> intProvider(){
        return Stream.of(Arguments.of(100,200),Arguments.of(5,10));
    }

    @ParameterizedTest
    @MethodSource({"intProvider"})
    void multipleSum2 (int i,int y) {
        Assertions.assertTrue(calculator.sum(i, y) > 0);
    }


}