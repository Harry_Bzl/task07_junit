package mockito_test;

public class BarVoid {
    FooVoid foo1;

    public BarVoid (FooVoid foo){
        this.foo1 = foo;
    }
    public void bar (String parameter){
        foo1.foo1(parameter);
    }
}
