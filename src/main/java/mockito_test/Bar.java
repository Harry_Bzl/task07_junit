package mockito_test;

public class Bar {
    private Foo foo;

    public Bar (Foo foo){
        this.foo = foo;
    }
    public String bar (String parameter){
        return foo.foo(parameter);
    }

    public String bar2 (String parameter){
        return parameter+"2";
    }
}
